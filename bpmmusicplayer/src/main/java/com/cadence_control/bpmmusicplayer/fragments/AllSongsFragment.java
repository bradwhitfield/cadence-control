package com.cadence_control.bpmmusicplayer.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.cadence_control.bpmmusicplayer.R;
import com.cadence_control.bpmmusicplayer.cursors.SongsFetcher;

/**
 * Created by brad on 11/17/14.
 */
public class AllSongsFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().getActionBar().setTitle(getString(R.string.drawer_all_songs));

        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                SongsFetcher.getAllSongs(getActivity().getContentResolver())));

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}