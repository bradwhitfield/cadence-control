package com.cadence_control.bpmmusicplayer.fragments;

import android.app.ListFragment;
import android.database.Cursor;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * This class will be removed soon! It is not as useful as originally intended.
 *
 * @author Brad Whtifield
 */
public class BaseListFragment extends ListFragment {
    //TODO: Make this handle more of the repetitive task from the listviews.

    protected void setupListView(int column, Cursor musicCursor) {
        ArrayList<String> viewArrayList = new ArrayList<>(musicCursor.getCount());

        //If anything was found, fill the ArrayList appropriately.
        if (musicCursor.moveToFirst()) {
            do {
                viewArrayList.add(musicCursor.getString(column));
            } while (musicCursor.moveToNext());
        }
        else {
            //Nothing found, so inform the user.
            viewArrayList = new ArrayList<>(1);
            viewArrayList.add("Nothing found.");
        }

        //Create the list adapter and bind it to our ListView
        //TODO: Either create custom adapter or use simple_list_item_2
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                viewArrayList));
    }
}
