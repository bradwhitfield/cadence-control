package com.cadence_control.bpmmusicplayer.fragments;

import android.content.Intent;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.cadence_control.bpmmusicplayer.BPMSongsActivity;
import com.cadence_control.bpmmusicplayer.R;
import com.cadence_control.bpmmusicplayer.cursors.BPMFetcher;

/**
 * A simple list fragment that shows all of the artist on a device. When the artist is tapped, then
 * a view containing what you have by that Artist is shown.
 *
 * @author Brad Whitfield
 */
public class BPMRangeFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Set the action bar title to the current view.
        //((ActionBarActivity)getActivity()).getSupportActionBar().setSubtitle(getString(R.string.drawer_bpm));

        //Get all of the artist on the device and setup the adapter
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                BPMFetcher.getBPMRanges(getActivity().getContentResolver())));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //Create a new intent that will display the songs
        Intent intent = new Intent(getActivity(), BPMSongsActivity.class);

        //Add the selected range to the extras in the intent.
        intent.putExtra(BPMSongsActivity.RANGE, (String) l.getItemAtPosition(position));

        startActivity(intent);
    }
}