package com.cadence_control.bpmmusicplayer.fragments;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.cadence_control.bpmmusicplayer.AlbumSongsActivity;
import com.cadence_control.bpmmusicplayer.R;
import com.cadence_control.bpmmusicplayer.cursors.AlbumsFetcher;
import com.cadence_control.bpmmusicplayer.mediamappings.Album;

/**
 * Displays all albums on the device through a basic ListView.
 *
 * @author Brad Whtifield
 */
public class AlbumFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getActionBar().setTitle(getString(R.string.drawer_album));

        //Create the list adapter and bind it to our ListView
        //TODO: Either create custom adapter or use simple_list_item_2
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                AlbumsFetcher.getAllAlbums(getActivity().getContentResolver())));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Listens for a user making a selection from the list. The ID and the Album Name are then passed to a new
     * Intent to display the list of songs on that album.
     *
     * @param l         Default parameter for Android's onListItemClick() method.
     * @param v         Default parameter for Android's onListItemClick() method.
     * @param position  Default parameter for Android's onListItemClick() method.
     * @param id        Default parameter for Android's onListItemClick() method.
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (((Album) l.getItemAtPosition(position)).getID() != -1) {
            Intent albumIntent = new Intent(getActivity(), AlbumSongsActivity.class);

            albumIntent.putExtra(AlbumSongsActivity.SELECTED_ALBUM,
                    ((Album) l.getItemAtPosition(position)).getAlbumName());

            albumIntent.putExtra(AlbumSongsActivity.SELECTED_ALBUM_ID,
                    ((Album) l.getItemAtPosition(position)).getID());

            startActivity(albumIntent);
        }
    }
}
