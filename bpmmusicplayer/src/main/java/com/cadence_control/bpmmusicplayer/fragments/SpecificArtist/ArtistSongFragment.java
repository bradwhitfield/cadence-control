package com.cadence_control.bpmmusicplayer.fragments.SpecificArtist;

import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.cadence_control.bpmmusicplayer.cursors.SongsFetcher;
import com.cadence_control.bpmmusicplayer.fragments.SpecifiedArtistFragment;

/**
 * Creates the ListFragment of the songs by an artist.
 *
 * @author Brad Whitfield
 */
public class ArtistSongFragment extends ListFragment {
    private String artist;

    /**
     * Creates the list of songs. This is a standard ListFragment. The extras passed to this method should
     * contain the artist_id of the artist that was selected in the parent activity.
     *
     * @param inflater              Default parameter for onCreateView
     * @param container             Default parameter for onCreateView
     * @param savedInstanceState    Default parameter for onCreateView
     * @return                      The view for Android to render.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle extras = getArguments();
        if (extras != null) {
            artist = extras.getString(SpecifiedArtistFragment.SELECTED_ARTIST);
        }

        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                SongsFetcher.getAllSongsFromArtist(getActivity().getContentResolver(), artist)));

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
