package com.cadence_control.bpmmusicplayer.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.cadence_control.bpmmusicplayer.R;
import com.cadence_control.bpmmusicplayer.cursors.GenresFetcher;

/**
 * Created by brad on 11/18/14.
 */
public class GenreFragment extends ListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Setup the title of the action bar to the current fragment
        getActivity().getActionBar().setTitle(getString(R.string.drawer_genre));

        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                GenresFetcher.getAllGenres(getActivity().getContentResolver())));

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
