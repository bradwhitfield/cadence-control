package com.cadence_control.bpmmusicplayer.fragments;

import android.app.ActionBar;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.cadence_control.bpmmusicplayer.R;
import com.cadence_control.bpmmusicplayer.cursors.ArtistsFetcher;
import com.cadence_control.bpmmusicplayer.mediamappings.Artist;

/**
 * A simple list fragment that shows all of the artist on a device. When the artist is tapped, then
 * a view containing what you have by that Artist is shown.
 *
 * @author Brad Whitfield
 */
public class ArtistFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Set the action bar title to the current view.
        ActionBar actionBar = getActivity().getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.drawer_artist));
        }

        //Get all of the artist on the device and setup the adapter
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                ArtistsFetcher.getAllArtist(getActivity().getContentResolver())));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Listens for a user making a selection from the list. The ID and the Artist Name are then passed to a new
     * Intent to display the list of albums and songs by that artist.
     *
     * @param l         Default parameter for Android's onListItemClick() method.
     * @param v         Default parameter for Android's onListItemClick() method.
     * @param position  Default parameter for Android's onListItemClick() method.
     * @param id        Default parameter for Android's onListItemClick() method.
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (((Artist) l.getItemAtPosition(position)).getID() != -1) {
            Intent artistIntent = new Intent(getActivity(), SpecifiedArtistFragment.class);

            artistIntent.putExtra(SpecifiedArtistFragment.SELECTED_ARTIST,
                    ((Artist) l.getItemAtPosition(position)).getArtistName());

            artistIntent.putExtra(SpecifiedArtistFragment.SELECTED_ARTIST_ID,
                    ((Artist) l.getItemAtPosition(position)).getID());

            startActivity(artistIntent);
            super.onListItemClick(l, v, position, id);
        }
    }
}
