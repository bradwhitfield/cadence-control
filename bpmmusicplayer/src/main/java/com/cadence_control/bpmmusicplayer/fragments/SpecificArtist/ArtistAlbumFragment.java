package com.cadence_control.bpmmusicplayer.fragments.SpecificArtist;

import android.content.Intent;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.cadence_control.bpmmusicplayer.cursors.AlbumsFetcher;
import com.cadence_control.bpmmusicplayer.AlbumSongsActivity;
import com.cadence_control.bpmmusicplayer.fragments.SpecifiedArtistFragment;
import com.cadence_control.bpmmusicplayer.mediamappings.Album;

/**
 * Creates the ListFragment of the Albums by an Artist.
 *
 * @author Brad Whitfield
 */
public class ArtistAlbumFragment extends ListFragment {
    private long artistID;

    /**
     * Creates the list of albums. This is a standard ListFragment. The extras passed to this method should
     * contain the artist_id of the artist that was selected in the parent activity.
     *
     * @param inflater              Default parameter for onCreateView
     * @param container             Default parameter for onCreateView
     * @param savedInstanceState    Default parameter for onCreateView
     * @return                      The view for Android to render.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle extras = getArguments();
        if (extras != null) {
            artistID = extras.getLong(SpecifiedArtistFragment.SELECTED_ARTIST_ID);
        }

        //Create the list adapter and bind it to our ListView
        setListAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_activated_1,
                AlbumsFetcher.getAllAlbumsFromArtist(getActivity().getContentResolver(), artistID)));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Listens for a user making a selection from the list. The ID and the Album Name are then passed to a new
     * Intent to display the list of songs on that album.
     *
     * @param l         Default parameter for Android's onListItemClick() method.
     * @param v         Default parameter for Android's onListItemClick() method.
     * @param position  Default parameter for Android's onListItemClick() method.
     * @param id        Default parameter for Android's onListItemClick() method.
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (((Album) l.getItemAtPosition(position)).getID() != -1) {
            Intent albumIntent = new Intent(getActivity(), AlbumSongsActivity.class);

            albumIntent.putExtra(AlbumSongsActivity.SELECTED_ALBUM,
                    ((Album) l.getItemAtPosition(position)).getAlbumName());

            albumIntent.putExtra(AlbumSongsActivity.SELECTED_ALBUM_ID,
                    ((Album) l.getItemAtPosition(position)).getID());

            startActivity(albumIntent);
        }
    }
}
