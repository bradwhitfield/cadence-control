package com.cadence_control.bpmmusicplayer.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import com.cadence_control.bpmmusicplayer.R;
import com.cadence_control.bpmmusicplayer.fragments.SpecificArtist.ArtistAlbumFragment;
import com.cadence_control.bpmmusicplayer.fragments.SpecificArtist.ArtistSongFragment;
import com.cadence_control.bpmmusicplayer.slidetabs.SlidingTabLayout;

/**
 * Creates a swipe-able page with the albums and songs of a specific artist in list views. The Songs and Albums are
 * divided up into separate fragments that query the appropriate resolvers.
 *
 * This is a simple implementation of the SlidingTabLayout provided by Google.
 *
 * @author Brad Whitfield
 */
public class SpecifiedArtistFragment extends FragmentActivity{
    public static final String SELECTED_ARTIST = "selected_artist";
    public static final String SELECTED_ARTIST_ID = "selected_artist_id";
    private String selectedArtist;
    private Long selectedArtistID;

    /**
     * When the view is created, the selected artist from the parent activity is stored, the ViewPager is created
     * (the sliding tab layout), and the two fragments containing the songs and albums are created.
     *
     * @param savedInstanceState    Android default parameter for onCreate() method
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_artist);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            selectedArtist = extras.getString(SELECTED_ARTIST);
            selectedArtistID = extras.getLong(SELECTED_ARTIST_ID);
        }

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(selectedArtist);
        }

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SlideViewFragmentPagerAdapter(getSupportFragmentManager(),
                SpecifiedArtistFragment.this, selectedArtist));

        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.tab_slide_text, 0);
        slidingTabLayout.setViewPager(viewPager);
    }

    /**
     * The adapter that handles the fragments being using in the ViewPager. This works like every other
     * FragmentPagerAdapter, except for the constructor.
     *
     * @author Brad Whitfield
     */
    public class SlideViewFragmentPagerAdapter extends FragmentPagerAdapter {
        private String[] tabTitles;
        private String selectedArtist;
        private Context context;

        /**
         * This is a similiar to every FragmentPagerAdapter constructor except it requires the application context in
         * order to grab the string resources for the title, and requires the selected artist so that each tab view
         * can grab the songs from the specified artist.
         *
         * @param fm                A FragmentManager instance.
         * @param context           The application context.
         * @param selectedArtist    The artist that the user selected in the parent activity.
         */
        public SlideViewFragmentPagerAdapter(FragmentManager fm, Context context, String selectedArtist) {
            super(fm);
            this.context = context;

            //Headings for the tabs
            tabTitles = getResources().getStringArray(R.array.slide_tab_strings);

            //The selected artist to sort by
            this.selectedArtist = selectedArtist;
        }

        /**
         * @return  The number of tabs in the view.
         */
        @Override
        public int getCount() {
            //The count will be equal to the number of elements in the String array that stores the tab titles
            return tabTitles.length;
        }

        /**
         * Selects the appropriate fragment based on the tab selected.
         *
         * @param tabPosition   The tab that was selected.
         * @return              A fragment that contains either the artist or album view.
         */
        @Override
        public Fragment getItem(int tabPosition) {
            Bundle args = new Bundle();
            args.putString(SELECTED_ARTIST, selectedArtist);
            args.putLong(SELECTED_ARTIST_ID, selectedArtistID);

            //This could be made more dynamic with an Enum, but
            //that is overly complicated for just these two tabs.
            if (tabPosition == 0) {
                Fragment fragment = new ArtistSongFragment();
                fragment.setArguments(args);
                return fragment;
            }
            else {
                Fragment fragment = new ArtistAlbumFragment();
                fragment.setArguments(args);
                return fragment;
            }
        }

        /**
         * Returns the page title of the current tab selected.
         *
         * These are gathered from the strings.xml file, so they are translatable.
         *
         * @param position  The position (list wise) of the tab in the FragmentPagerAdapter.
         * @return          The title of the selected tab.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
