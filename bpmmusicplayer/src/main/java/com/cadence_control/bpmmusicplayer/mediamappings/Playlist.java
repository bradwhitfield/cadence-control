package com.cadence_control.bpmmusicplayer.mediamappings;

/**
 * A simple class that keeps track of the information needed for each playlist.
 *
 * @author Brad Whitfield
 */
public class Playlist {
    private long ID;
    private String playlist;

    public Playlist(long ID, String playlist) {
        this.ID = ID;
        this.playlist = playlist;
    }

    public long getID() {
        return ID;
    }

    public String getPlaylistName() {
        return playlist;
    }

    @Override
    public String toString() {
        return this.playlist;
    }
}
