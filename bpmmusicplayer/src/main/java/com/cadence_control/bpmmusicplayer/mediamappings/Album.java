package com.cadence_control.bpmmusicplayer.mediamappings;

/**
 * A simple class that keeps track of the information needed for each album.
 *
 * @author Brad Whitfield
 */
public class Album {
    private long ID;
    private String album;
    private String artist;

    public Album(long ID, String album, String artist) {
        this.ID = ID;
        this.album = album;
        this.artist = artist;
    }

    public long getID() {
        return ID;
    }

    public String getAlbumName() {
        return album;
    }

    public String getArtist() {
        return artist;
    }

    @Override
    public String toString() {
        return this.album;
    }
}
