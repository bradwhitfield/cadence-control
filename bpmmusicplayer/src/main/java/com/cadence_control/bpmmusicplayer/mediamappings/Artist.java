package com.cadence_control.bpmmusicplayer.mediamappings;

/**
 * A simple class that keeps track of the information needed for each artist.
 *
 * @author Brad Whitfield
 */
public class Artist {
    private long ID;
    private String artist;

    public Artist(long ID, String artist) {
        this.ID = ID;
        this.artist = artist;
    }

    public long getID() {
        return ID;
    }

    public String getArtistName() {
        return artist;
    }

    @Override
    public String toString() {
        return this.artist;
    }
}
