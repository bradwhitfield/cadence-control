package com.cadence_control.bpmmusicplayer.mediamappings;

/**
 * A simple class that keeps track of the information needed for each song.
 *
 * @author Brad Whitfield
 */
public class Song {
    private long ID;
    private int BPM;
    private int trackNum;
    private String artist;
    private String album;
    private String title;

    public Song(long ID, int trackNum, String artist, String album, String title) {
        this.ID = ID;
        this.trackNum = trackNum;
        this.artist = artist;
        this.album = album;
        this.title = title;
    }

    public Song(long ID, int trackNum, String artist, String album, String title, int bpm) {
        this.ID = ID;
        this.trackNum = trackNum;
        this.artist = artist;
        this.album = album;
        this.title = title;
        this.BPM = bpm;
    }

    public long getID() {
        return ID;
    }

    public int getBPM() {
        return BPM;
    }

    public int getTrackNum() {
        return trackNum;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getTitle() {
        return title;
    }

    public void setBPM(int bpm) {
        this.BPM = bpm;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
