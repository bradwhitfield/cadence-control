package com.cadence_control.bpmmusicplayer.mediamappings;

/**
 * A simple class that keeps track of the information needed for each genre.
 *
 * @author Brad Whitfield
 */
public class Genre {
    private long ID;
    private String name;

    public Genre(long ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public long getID() {
        return ID;
    }

    public String getGenreName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
