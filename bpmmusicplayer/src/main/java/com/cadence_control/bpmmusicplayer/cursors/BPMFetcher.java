package com.cadence_control.bpmmusicplayer.cursors;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import com.cadence_control.bpmmusicplayer.bpm.BPMProvider;
import com.cadence_control.bpmmusicplayer.mediamappings.Song;

import java.util.ArrayList;

/**
 * This class contains the methods used to grab an ArrayList of all possible BPM ranges for the songs on the device.
 *
 * @author Brad Whitfield
 */
public class BPMFetcher {

    /**
     * Get ranges of BPMs in tens (90-99, 120-129, etc...). These are represented as Strings for the navigation drawer.
     *
     * @param contentResolver   A ContentResolver associated with the application context.
     * @return                  The string values for the navigation drawer that represent possible BPM ranges.
     */
    public static ArrayList<String> getBPMRanges(ContentResolver contentResolver) {
        Cursor cursor = contentResolver.query(BPMProvider.CONTENT_URL, new String[] {"DISTINCT " + BPMProvider.RANGE},
                null, null, BPMProvider.RANGE);

        ArrayList<String> ranges = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                if (cursor.getInt(0) != 0) {
                    ranges.add(cursor.getString(0) + " - " + (cursor.getInt(0) + 9) );
                }
            } while (cursor.moveToNext());
        }

        cursor.close();

        return ranges;
    }

    /**
     * Builds a basic query to get the BPM of a song in the database. Returns -1 if no BPM was found.
     *
     * @param songID    The ID given to the song by Android when it was first put on the device.
     * @return          The BPM stored for the song.
     */
    public static int getSongBPM(ContentResolver contentResolver, Long songID) {
        int lookupBPM = -1;

        //Returns the BPM column for the song with passed in ID.
        Cursor cursor = contentResolver.query(BPMProvider.CONTENT_URL, new String[]{BPMProvider.BPM},
                BPMProvider.ID + "=?", new String[]{songID.toString()}, null);

        if (cursor.moveToFirst()) {
            //Since we are only returning the one column, it is safe to use the index.
            lookupBPM = cursor.getInt(0);
        }
        cursor.close();
        return lookupBPM;
    }

    /**
     * Inserts a single song and BPM into the database.
     *
     * @param contentResolver   The applications ContentResolver to access the ContentProvider.
     * @param song              The song to be stored in the databaser with the BPM.
     */
    public static void insertBPM(ContentResolver contentResolver, Song song) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BPMProvider.ID, song.getID());
        contentValues.put(BPMProvider.BPM, song.getBPM());

        //Get the tens place range for the song
        contentValues.put(BPMProvider.RANGE, (song.getBPM() / 10 * 10));

        contentValues.put(BPMProvider.TRACK_NUM, song.getTrackNum());
        contentValues.put(BPMProvider.ARTIST, song.getArtist());
        contentValues.put(BPMProvider.ALBUM, song.getAlbum());
        contentValues.put(BPMProvider.TITLE, song.getTitle());

        contentResolver.insert(BPMProvider.CONTENT_URL, contentValues);
    }

    /**
     * Gets all songs in a specific BPM range on the device.
     *
     * @param contentResolver   The applications ContentResolver to access the ContentProvider.
     * @param range             The range being looked for.
     * @return                  A list of the songs in the BPM range.
     */
    public static ArrayList<Song> getSongsInBPMRange(ContentResolver contentResolver, String range) {
        Cursor cursor = contentResolver.query(BPMProvider.CONTENT_URL,
                new String[] {BPMProvider.ID, BPMProvider.BPM, BPMProvider.TRACK_NUM, BPMProvider.ARTIST,
                        BPMProvider.ALBUM, BPMProvider.TITLE},
                BPMProvider.RANGE + "=?", new String[] {range}, null);

        return createListFromIDs(contentResolver, cursor);
    }

    /**
     * Gets all songs in a specific BPM range on the device.
     *
     * @param contentResolver   The applications ContentResolver to access the ContentProvider.
     * @param range             The range being looked for.
     * @return                  A list of the songs in the BPM range.
     */
    public static ArrayList<Song> getSongsInBPMRange(ContentResolver contentResolver, int range) {
        return BPMFetcher.getSongsInBPMRange(contentResolver, String.valueOf(range));
    }

    private static ArrayList<Song> createListFromIDs(ContentResolver contentResolver, Cursor cursor) {
        ArrayList<Song> songs = new ArrayList<>();
        int idColumn = cursor.getColumnIndex(BPMProvider.ID);
        int bpmColumn = cursor.getColumnIndex(BPMProvider.BPM);
        int trackColumn = cursor.getColumnIndex(BPMProvider.TRACK_NUM);
        int artistColumn = cursor.getColumnIndex(BPMProvider.ARTIST);
        int albumColumn = cursor.getColumnIndex(BPMProvider.ALBUM);
        int titleColumn = cursor.getColumnIndex(BPMProvider.TITLE);

        if (cursor.moveToFirst()) {
            do {
                songs.add(new Song(cursor.getLong(idColumn), cursor.getInt(trackColumn),
                        cursor.getString(artistColumn), cursor.getString(albumColumn),
                        cursor.getString(titleColumn), cursor.getInt(bpmColumn)));
            } while (cursor.moveToNext());
        }
        cursor.close();

        return songs;
    }
}
