package com.cadence_control.bpmmusicplayer.cursors;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import com.cadence_control.bpmmusicplayer.mediamappings.Playlist;

import java.util.ArrayList;

/**
 * This class contains the methods used to grab an ArrayList of all playlist on the device.
 *
 * @author Brad Whitfield
 */
public class PlaylistFetcher {
    /**
     * Gets all of the playlist on a device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context
     * @return                 An ArrayList containing all the playlist on a device.
     */
    public static ArrayList<Playlist> getAllPlaylist(ContentResolver contentResolver) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[] {BaseColumns._ID, MediaStore.Audio.PlaylistsColumns.NAME},
                null, null, MediaStore.Audio.PlaylistsColumns.NAME);

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int playlistColumn = cursor.getColumnIndex(MediaStore.Audio.PlaylistsColumns.NAME);

        ArrayList<Playlist> playlistArrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                playlistArrayList.add(new Playlist(cursor.getLong(IDColumn), cursor.getString(playlistColumn)));
            } while (cursor.moveToNext());
        }
        else {
            //Nothing found, so inform the user
            playlistArrayList.add(new Playlist(-1, "Nothing Found!"));
        }

        cursor.close();

        return playlistArrayList;
    }
}
