package com.cadence_control.bpmmusicplayer.cursors;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import com.cadence_control.bpmmusicplayer.mediamappings.Genre;

import java.util.ArrayList;

/**
 * This class contains the methods used to grab an ArrayList of all genres on the device.
 *
 * @author Brad Whitfield
 */
public class GenresFetcher {

    /**
     * Gets all of the genres on a device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context
     * @return                 An ArrayList containing all the genres on a device.
     */
    public static ArrayList<Genre> getAllGenres(ContentResolver contentResolver) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
                new String[] {BaseColumns._ID, MediaStore.Audio.Genres.NAME},
                null, null, MediaStore.Audio.Genres.NAME);

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int nameColumn = cursor.getColumnIndex(MediaStore.Audio.Genres.NAME);

        ArrayList<Genre> genreList = new ArrayList<>();
        //If anything was found, fill the ArrayList appropriately.
        if (cursor.moveToFirst()) {
            do {
                genreList.add(new Genre(cursor.getLong(IDColumn), cursor.getString(nameColumn)));
            } while (cursor.moveToNext());
        }
        else {
            //Nothing found, so inform the user.
            genreList.add(new Genre(-1, "Nothing Found!"));
        }

        cursor.close();

        return genreList;
    }
}
