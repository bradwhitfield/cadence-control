package com.cadence_control.bpmmusicplayer.cursors;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import com.cadence_control.bpmmusicplayer.mediamappings.Artist;

import java.util.ArrayList;

/**
 * This class contains the methods used to grab an ArrayList of all artist on the device.
 *
 * @author Brad Whitfield
 */
public class ArtistsFetcher {
    /**
     * Gets all of the artists on a device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context
     * @return                 An ArrayList containing all the artist on a device.
     */
    public static ArrayList<Artist> getAllArtist(ContentResolver contentResolver) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
                new String[] {BaseColumns._ID,
                    MediaStore.Audio.Media.ARTIST},
                null, null, MediaStore.Audio.Media.ARTIST);

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);

        ArrayList<Artist> artistsList = new ArrayList<>();
        //If anything was found, fill the ArrayList appropriately.
        if (cursor.moveToFirst()) {
            do {
                artistsList.add(new Artist(cursor.getLong(IDColumn), cursor.getString(artistColumn)));
            } while (cursor.moveToNext());
        }
        else {
            //Nothing found, so inform the user.
            artistsList.add(new Artist(-1, "Nothing Found!"));
        }

        cursor.close();

        return artistsList;
    }
}
