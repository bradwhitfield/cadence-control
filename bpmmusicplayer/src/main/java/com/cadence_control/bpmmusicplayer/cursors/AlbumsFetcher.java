package com.cadence_control.bpmmusicplayer.cursors;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import com.cadence_control.bpmmusicplayer.mediamappings.Album;

import java.util.ArrayList;

/**
 * This class contains the methods used to grab an ArrayList of albums on the device,
 * and the albums of a specific artist on a device.
 *
 * @author Brad Whitfield
 */
public class AlbumsFetcher {
    /**
     * Gets all albums on a device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context
     * @return                 An ArrayList containing all the albums on a device.
     */
    public static ArrayList<Album> getAllAlbums(ContentResolver contentResolver){
        Cursor cursor = contentResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[] {BaseColumns._ID,
                        MediaStore.Audio.AlbumColumns.ALBUM,
                        MediaStore.Audio.AlbumColumns.ARTIST},
                null, null, MediaStore.Audio.AlbumColumns.ALBUM);

        return createList(cursor);
    }

    /**
     * Gets all albums from a specific artist on a device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context
     * @param artistID         The ID android assigns to a selected artist.
     * @return                 An ArrayList containing all the albums on a device.
     */
    public static ArrayList<Album> getAllAlbumsFromArtist(ContentResolver contentResolver, long artistID) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Artists.Albums.getContentUri("external", artistID),
                new String[] {BaseColumns._ID,
                        MediaStore.Audio.AlbumColumns.ALBUM,
                        MediaStore.Audio.AlbumColumns.ARTIST},
                null, null, MediaStore.Audio.AlbumColumns.ALBUM);

        return createList(cursor);
    }

    private static ArrayList<Album> createList(Cursor cursor) {
        ArrayList<Album> albumsList = new ArrayList<>(cursor.getCount());

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ARTIST);

        //If anything was found, fill the ArrayList appropriately.
        if (cursor.moveToFirst()) {
            do {
                albumsList.add(new Album(cursor.getLong(IDColumn),
                        cursor.getString(albumColumn),
                        cursor.getString(artistColumn)));
            } while (cursor.moveToNext());
        }
        else {
            //Nothing found, so inform the user.
            albumsList = new ArrayList<>(1);
            albumsList.add(new Album(-1, "Nothing Found!", "Nothing Found!"));
        }

        cursor.close();

        return albumsList;
    }
}
