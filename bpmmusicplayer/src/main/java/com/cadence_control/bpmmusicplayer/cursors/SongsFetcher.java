package com.cadence_control.bpmmusicplayer.cursors;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import com.cadence_control.bpmmusicplayer.bpm.BPMProvider;
import com.cadence_control.bpmmusicplayer.mediamappings.Song;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * This class contains the methods used to grab an ArrayList of all songs,
 * the songs on a specific album, and the songs of a specific artist on the device.
 *
 * @author Brad Whitfield
 */
public class SongsFetcher {
    private static final String[] selectionColumns = {BaseColumns._ID, MediaStore.Audio.Media.TRACK,
            MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.TITLE};
    /**
     * Get all songs on the device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context.
     * @return                 An ArrayList containing all the songs on a device.
     */
    public static ArrayList<Song> getAllSongs(ContentResolver contentResolver) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                selectionColumns,
                MediaStore.Audio.AudioColumns.IS_MUSIC + "=1 AND " + MediaStore.Audio.AudioColumns.TITLE + " != ''",
                null, MediaStore.Audio.Media.ARTIST);

        return createList(contentResolver, cursor);
    }

    /**
     * Gets all songs from a specific artist on the device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context.
     * @param artist           The artist name being searched for.
     * @return                 An ArrayList containing all the songs by an artist on the device.
     */
    public static ArrayList<Song> getAllSongsFromArtist(ContentResolver contentResolver, String artist) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                selectionColumns, MediaStore.Audio.Media.ARTIST + "=?", new String[] {artist},
                MediaStore.Audio.Media.TITLE);

        return createList(cursor);
    }

    /**
     * Gets all songs on a specific album on the device.
     *
     * @param contentResolver  A ContentResolver passed from an Activity (or something) with Context.
     * @param albumID          The android assigned ID for the album being selected.
     * @return                 An ArrayList containing all the songs in the specified album.
     */
    public static ArrayList<Song> getAllSongsOnAlbum(ContentResolver contentResolver, Long albumID) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                selectionColumns, MediaStore.Audio.AudioColumns.ALBUM_ID + "=?",
                new String[] {albumID.toString()}, MediaStore.Audio.Media.TRACK);

        return createList(cursor);
    }

    public static Song getSongByID(ContentResolver contentResolver, Long songID, Integer bpm) {
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                selectionColumns, BaseColumns._ID + "=?", new String[] {String.valueOf(songID)}, null);

        return createSong(cursor, bpm);
    }

    public static ArrayList<Song> getSongInIDSet(ContentResolver contentResolver, HashSet<Long> songs) {
        //Construct a where in clause
        StringBuilder sb = new StringBuilder();

        sb.append("('");

        for (Long id: songs) {
            sb.append(id.toString() + "', '");
        }

        sb.delete(sb.lastIndexOf(","), sb.lastIndexOf("'") + 1);
        sb.append(")");

        //Get all songs with any of the passed in IDs.
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                selectionColumns, BaseColumns._ID + " IN " + sb.toString(), null, null);

        return createList(cursor);
    }


    private static ArrayList<Song> createList(Cursor cursor) {
        ArrayList<Song> songsList = new ArrayList<>(cursor.getCount());

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int trackColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);

        //If anything was found, fill the ArrayList with songs
        if (cursor.moveToFirst()) {
            do {
                songsList.add(new Song(cursor.getLong(IDColumn), cursor.getInt(trackColumn),
                        cursor.getString(artistColumn), cursor.getString(albumColumn),
                        cursor.getString(titleColumn)));
            } while (cursor.moveToNext());
        }
        else {
            //Nothing found, so inform the users.
            songsList.add(new Song(-1, 0, "Nothing Found!", "Nothing Found!", "Nothing Found!"));
        }

        cursor.close();

        return songsList;
    }

    private static ArrayList<Song> createList(ContentResolver contentResolver, Cursor cursor) {
        ArrayList<Song> songsList = new ArrayList<>(cursor.getCount());

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int trackColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);

        //If anything was found, fill the ArrayList with songs
        if (cursor.moveToFirst()) {
            do {
                songsList.add(new Song(cursor.getLong(IDColumn), cursor.getInt(trackColumn),
                        cursor.getString(artistColumn), cursor.getString(albumColumn),
                        cursor.getString(titleColumn), BPMFetcher.getSongBPM(contentResolver, cursor.getLong(IDColumn))));
            } while (cursor.moveToNext());
        }
        else {
            //Nothing found, so inform the users.
            songsList.add(new Song(-1, 0, "Nothing Found!", "Nothing Found!", "Nothing Found!", -1));
        }

        cursor.close();

        return songsList;
    }

    private static Song createSong(Cursor cursor, Integer bpm) {
        Song song;

        int IDColumn = cursor.getColumnIndex(BaseColumns._ID);
        int trackColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
        int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int albumColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
        int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);

        //If anything was found, fill the ArrayList with songs
        if (cursor.moveToFirst()) {
            song = new Song(cursor.getLong(IDColumn), cursor.getInt(trackColumn),
                    cursor.getString(artistColumn), cursor.getString(albumColumn),
                    cursor.getString(titleColumn), bpm);
        }
        else {
            //Nothing found, so inform the users.
            song =  new Song(-1, 0, "Nothing Found!", "Nothing Found!", "Nothing Found!");
        }

        cursor.close();

        return song;
    }
}