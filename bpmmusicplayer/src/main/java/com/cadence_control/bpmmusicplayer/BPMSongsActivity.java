package com.cadence_control.bpmmusicplayer;

import android.app.ListActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import com.cadence_control.bpmmusicplayer.cursors.BPMFetcher;
import com.cadence_control.bpmmusicplayer.mediamappings.Song;

/**
 * Created by brad on 3/23/15.
 */
public class BPMSongsActivity extends ListActivity {
    public static final String RANGE = "range";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String range;
        Bundle extras = getIntent().getExtras();

        if (extras != null){
            range = extras.getString(RANGE);
        }
        else {
            range = "";
        }

        String bpmrange = range.substring(0, range.indexOf(" "));

        //((ActionBarActivity) getApplicationContext()).getSupportActionBar().setSubtitle(range);

        setListAdapter(new ArrayAdapter<Song>(this, android.R.layout.simple_list_item_activated_1,
                BPMFetcher.getSongsInBPMRange(this.getContentResolver(), bpmrange)));

        super.onCreate(savedInstanceState);
    }
}
