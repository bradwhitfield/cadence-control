package com.cadence_control.bpmmusicplayer;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.app.DialogFragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.support.v4.widget.DrawerLayout;
import com.cadence_control.bpmmusicplayer.bpm.CheckForNewSongs;
import com.cadence_control.bpmmusicplayer.dialogs.FirstRunDialog;
import com.cadence_control.bpmmusicplayer.fragments.*;

public class SongView extends Activity {
    private static Context context;

    private ListView drawerListView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_navigation_drawer);

        SharedPreferences sharedPreferences =  getPreferences(MODE_PRIVATE);
        boolean firstRun = sharedPreferences.getBoolean("first_run", true);

        if (firstRun) {

            DialogFragment firstRunDialog = new FirstRunDialog();
            firstRunDialog.show(getFragmentManager(), "first_run");

            SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
            preferencesEditor.putBoolean("first_run", false);

            preferencesEditor.apply();
        }
        else {
            //Start the task that will check for new songs on the device.
            new CheckForNewSongs(getApplicationContext().getContentResolver()).execute();
        }

        context = getApplicationContext();

        ActionBar actionBar = getActionBar();

        //List of our items in navigation drawer
        ArrayList<String> navigationDrawerStrings = SortAttributes.getNavigationStrings();


        //Get the needed layout for the drawer and main list
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerListView = (ListView) findViewById(R.id.navigation_drawer);

        //Use the array of navigation strings to create and adapter
        //and bind it to our navigation drawer, creating our ListView in the drawer
        drawerListView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, navigationDrawerStrings));

        //Set the default behaviour for toggling the drawer
        //Essentially an event listener
        //Google's documentation explains this pretty well
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        //Some needed event listeners
        drawerLayout.setDrawerListener(drawerToggle);
        drawerListView.setOnItemClickListener(new DrawerItemClickListener());

        //TODO: maybe
        selectItem(1);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawerLayout.isDrawerOpen(drawerListView);

        //TODO:Hide actionbar items as appropriate, such as below
        //menu.findItem(R.id.action_songsearch).setVisible(!drawerOpen);

        return super.onPrepareOptionsMenu(menu);
    }

    public class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                //0 Is run mode, so skip that for now.
                selectItem(position);
            }
            else {
                drawerLayout.closeDrawers();
            }
        }
    }

    private void selectItem(int position) {
        //Create a new fragment that corresponds to the correct sorting method
        Fragment fragment;
        Bundle arguments = new Bundle();

        fragment = Fragment.instantiate(this, SortAttributes.getFragment(position).getName());

        //Insert the fragment (attribute view) and replace current
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.song_list_frame, fragment).commit();

        //Highlight the chosen item, update the app title, and close the drawer
        drawerListView.setItemChecked(position, true);
        drawerLayout.closeDrawer(drawerListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_song_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        else if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_exit) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public enum SortAttributes {
        //Run Mode does not require a UI element (Fragment)
        RUN_MODE(context.getString(R.string.drawer_run_mode), 0, null),
        ARTIST(context.getString(R.string.drawer_artist), 1, ArtistFragment.class),
        ALBUM(context.getString(R.string.drawer_album), 2, AlbumFragment.class),
        BPM(context.getString(R.string.drawer_bpm), 3, BPMRangeFragment.class),
        PLAYLIST(context.getString(R.string.drawer_playlist), 4, PlaylistFragment.class),
        GENRE(context.getString(R.string.drawer_genre), 5, GenreFragment.class),
        ALL_SONGS(context.getString(R.string.drawer_all_songs), 6, AllSongsFragment.class);

        private final String attribute;
        private final int position;
        private final Class<? extends Fragment> fragment;

        SortAttributes(String attribute, int position, Class<? extends Fragment> fragment) {
            this.attribute = attribute;
            this.position = position;
            this.fragment = fragment;
        }

        //Make a HashMap for quicker lookup
        private static Map<Integer, SortAttributes> lookup = new HashMap<Integer, SortAttributes>();
        static {
            for (SortAttributes a: SortAttributes.values()) {
                lookup.put(a.position, a);
            }
        }

        public static Class<? extends Fragment> getFragment(int position) {
            return lookup.get(position).fragment;
        }

        public static ArrayList<String> getNavigationStrings() {

            //This will be a list of all the various options in the navigation drawer
            ArrayList<String> listViewStrings = new ArrayList<String>();

            for (SortAttributes a: SortAttributes.values()) {
                listViewStrings.add(a.attribute);
            }

            return listViewStrings;
        }
    }
}