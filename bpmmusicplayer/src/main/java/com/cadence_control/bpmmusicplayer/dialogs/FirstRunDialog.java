package com.cadence_control.bpmmusicplayer.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.app.DialogFragment;
import com.cadence_control.bpmmusicplayer.bpm.EchonestLookup;
import com.cadence_control.bpmmusicplayer.cursors.SongsFetcher;
import com.cadence_control.bpmmusicplayer.mediamappings.Song;

import java.util.ArrayList;

/**
 * When the user launches the application for the first time, or after clearing the application data,
 * this dialog will appear to them. The dialog will prompt the user to allow the app to lookup BPM for
 * the songs on the device.
 *
 * @author Brad Whtifield
 */
public class FirstRunDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("No BPM data found!");
        builder.setMessage("Would you like to run a lookup in the background to the BPM for the songs on your device?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayList<Song> songs = SongsFetcher.getAllSongs(getActivity().getContentResolver());
                new EchonestLookup(getActivity().getApplicationContext().getContentResolver())
                        .execute(songs.toArray(new Song[songs.size()]));
                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("first_run", false);
                editor.apply();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("first_run", true);
                editor.apply();
            }
        });

        return builder.create();
    }
}
