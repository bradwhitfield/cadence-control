package com.cadence_control.bpmmusicplayer.bpm;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.HashMap;

/**
 * A very simple content provider that provides BPM for songs.
 *
 * @author Brad Whitfield
 */
public class BPMProvider extends ContentProvider {

    /**
     * The full name of the provider
     */
    public static final String PROVIDER_NAME = "com.cadence_control.bpmmusicplayer.bpm.BPMProvider";
    /**
     * The URL to our BPM provider
     */
    public static final String URL = "content://" + PROVIDER_NAME + "/bpm";
    /**
     * The Uri to the stored BPM
     */
    public static final Uri CONTENT_URL = Uri.parse(URL);

    /**
     * Column name for ID
     */
    public static final String ID = "id";
    /**
     * Column name for bpm
     */
    public static final String BPM = "bpm";
    /**
     * Column name for the range the BPM falls in
     */
    public static final String RANGE = "range";
    /**
     * Column name for the track number
     */
    public static final String TRACK_NUM = "tracknum";
    /**
     * Column name for the artist
     */
    public static final String ARTIST = "artist";
    /**
     * Column name for the album
     */
    public static final String ALBUM = "album";
    /**
     * Column name for the song title
     */
    public static final String TITLE = "title";

    private static final int uriCode = 1;
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "bpm", uriCode);
    }

    private SQLiteDatabase sqLiteDB;
    private static final String DATABASE_NAME = "songsBPM";
    private static final String TABLE_NAME = "songs";
    private static final int DATABASE_VERSION = 1;
    private static final String CREATE_DB_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (id INT8 PRIMARY KEY, bpm INTEGER, range INTEGER, tracknum INTEGER, artist TEXT, " +
            "album TEXT, title TEXT);";

    private static HashMap<String, String> values;

    /**
     * Creates a connection to the SQLite database.
     *
     * @return  If a connection could be made to the SQLite Database
     */
    public boolean onCreate() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        sqLiteDB = databaseHelper.getWritableDatabase();
        return sqLiteDB != null;
    }

    /**
     * A basic implementation of the query method implemented in all Android Content Providers.
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case uriCode:
                queryBuilder.setProjectionMap(values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        Cursor cursor = queryBuilder.query(sqLiteDB, projection, selection, selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    /**
     * A basic implementation of the getType method implemented in all Android Content Providers.
     */
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case uriCode:
                return "vnd.android.cursor.dir/bpm";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    /**
     * A basic implementation of the insert method implemented in all Android Content Providers.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID = sqLiteDB.insert(TABLE_NAME, null, values);

        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URL, rowID);

            getContext().getContentResolver().notifyChange(_uri, null);

            return _uri;
        }
        return null;
    }

    /**
     * A basic implementation of the delete method implemented in all Android Content Providers.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int rowDeleted;

        switch (uriMatcher.match(uri)){
            case uriCode:
                rowDeleted = sqLiteDB.delete(TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowDeleted;
    }

    /**
     * A basic implementation of the update method implemented in all Android Content Providers.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int rowUpdated;

        switch (uriMatcher.match(uri)) {
            case uriCode:
                rowUpdated = sqLiteDB.update(TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowUpdated;
    }

    //Simple extension of the SQLiteOpenHelper class so we can implement changes to the database later, if necessary.
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDB) {
            sqLiteDB.execSQL(BPMProvider.CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDB, int oldVersion, int newVersion) {
            //This is not what should be done during an actual upgrade.
            sqLiteDB.execSQL("DROP TABLE IF EXIST " + TABLE_NAME);
            onCreate(sqLiteDB);
        }
    }
}
