package com.cadence_control.bpmmusicplayer.bpm;

import android.content.ContentResolver;
import android.os.AsyncTask;
import com.cadence_control.bpmmusicplayer.cursors.BPMFetcher;
import com.cadence_control.bpmmusicplayer.mediamappings.Song;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URLEncoder;

/**
 * A simple AsyncTask (soon to be a service, I think) that takes an array of songs and looks
 * up the BPM in EchoNest, and then stores the returned result (if any) in the database.
 *
 * @author Brad Whitfield
 */
public class EchonestLookup extends AsyncTask<Song, Void, Void> {
    private static final String songLookupUrl =
            "http://developer.echonest.com/api/v4/song/search?api_key=MFN44EK5YIBY0TE4M";
    private static final String bpmLookupUrl =
            "http://developer.echonest.com/api/v4/song/profile?api_key=MFN44EK5YIBY0TE4M&id=";
    private static final String artistParam = "&artist=";
    private static final String titleParam = "&title=";
    private static final String audioSummary = "&bucket=audio_summary";

    private ContentResolver contentResolver;

    /**
     * Creates the Async task with the ContentResolver so that the information can be stored in the database.
     *
     * @param contentResolver   A ContentResolver from the application context.
     */
    public EchonestLookup(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    protected Void doInBackground(Song... params) {

        for (Song song: params) {
            try {
                String url = songLookupUrl + artistParam
                        + URLEncoder.encode(song.getArtist(), "UTF-8").replace("+", "%20")
                        + titleParam + URLEncoder.encode(song.getTitle(), "UTF-8").replace("+", "%20");

                JSONObject jsonObject = getJSONFromQuery(url);

                int code = jsonObject.getJSONObject("response").getJSONObject("status").getInt("code");

                if (code == 0) {
                    JSONArray songInfo = jsonObject.getJSONObject("response").getJSONArray("songs");
                    String songID = songInfo.getJSONObject(0).getString("id");

                    url = bpmLookupUrl + songID + audioSummary;

                    jsonObject = getJSONFromQuery(url);

                    code = jsonObject.getJSONObject("response").getJSONObject("status").getInt("code");

                    if (code == 0) {
                        double tempo = jsonObject.getJSONObject("response").getJSONArray("songs").
                                getJSONObject(0).getJSONObject("audio_summary").getDouble("tempo");
                        song.setBPM((int) Math.round(tempo));
                        BPMFetcher.insertBPM(contentResolver, song);
                    }
                    else if (code == 3) {
                        //Using basic version of Echonest API at the moment, so limit the number of calls to the server.
                        Thread.sleep(60000);
                    }
                    else {
                        BPMFetcher.insertBPM(contentResolver, song);
                    }
                }
                else if (code == 3) {
                    //Using basic version of Echonest API at the moment, so limit the number of calls to the server.
                    Thread.sleep(60000);
                }
                else {
                    BPMFetcher.insertBPM(contentResolver, song);
                }

            } catch (ClientProtocolException e) {
                //TODO: Make these catches useful
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    //TODO: Figure out what APIs Google replaced the deprecated ones with.
    private JSONObject getJSONFromQuery(String url) throws IOException, JSONException {
        DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-type", "text/json");

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        InputStream inputStream = entity.getContent();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }

        return new JSONObject(sb.toString());
    }
}