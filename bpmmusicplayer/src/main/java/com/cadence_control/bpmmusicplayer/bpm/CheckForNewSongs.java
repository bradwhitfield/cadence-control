package com.cadence_control.bpmmusicplayer.bpm;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import com.cadence_control.bpmmusicplayer.cursors.SongsFetcher;
import com.cadence_control.bpmmusicplayer.mediamappings.Song;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * A service to run any time the app is launched that will check for new songs on the device.
 * New songs are detected by comparing the Android's database of songs to the one created by
 * Cadence Control. Only the Android-assigned IDs are compared as Sets in an attempt to make
 * the comparison as fast as possible.
 *
 * When new songs are detected on the Android device, those songs are sent to the applications
 * default method for BPM detection to determine what the BPM of the songs are and add them
 * to the application's database.
 *
 * When songs are removed from the device, the songs are removed from the application's database.
 *
 * @author Brad Whtifield
 */
public class CheckForNewSongs extends AsyncTask<Void, Void, Void> {
    private ContentResolver contentResolver;

    public CheckForNewSongs(ContentResolver contentResolver) {
        super();
        this.contentResolver = contentResolver;
    }

    @Override
    protected Void doInBackground(Void... params) {
        HashSet<Long> deviceSongs = new HashSet<>();
        HashSet<Long> knownSongs = new HashSet<>();

        //These will not go in the Fetcher classes since they will only be used once, and they are unique cases

        //Get all song IDs on the Android device
        Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] {BaseColumns._ID},
                MediaStore.Audio.AudioColumns.IS_MUSIC + "=1 AND " + MediaStore.Audio.AudioColumns.TITLE + " != ''",
                null, BaseColumns._ID);

        if (cursor.moveToFirst()) {
            do {
                deviceSongs.add(cursor.getLong(0));
            } while (cursor.moveToNext());
        }
        cursor.close();

        //Get all song IDs in the BPM database
        cursor = contentResolver.query(BPMProvider.CONTENT_URL, new String[] {BPMProvider.ID},
                null, null, BPMProvider.ID);

        if (cursor.moveToFirst()) {
            do {
                knownSongs.add(cursor.getLong(0));
            } while (cursor.moveToNext());
        }
        cursor.close();

        //If there are songs that we have in the BPM database, but that aren't in Androids database,
        //then the songs have been removed from the device, so remove them from the BPM database.
        if (!deviceSongs.containsAll(knownSongs)) {
            HashSet<Long> toBeRemoved = new HashSet<>(knownSongs);
            toBeRemoved.removeAll(deviceSongs);

            //Remove each ID from the database
            for (Long id : toBeRemoved) {
                contentResolver.delete(BPMProvider.CONTENT_URL, BPMProvider.ID, new String[]{id.toString()});
            }
        }

        //If there are songs that Android knows about that aren't in the BPM database,
        //then new songs were added since last time the app was opened, so determine the BPM for these.
        if (!knownSongs.containsAll(deviceSongs)) {
            HashSet<Long> toBeAdded = new HashSet<>(deviceSongs);
            toBeAdded.removeAll(knownSongs);

            //Create ArrayList of songs and pass it to the lookup.
            ArrayList<Song> songs = SongsFetcher.getSongInIDSet(contentResolver, toBeAdded);
            new EchonestLookup(contentResolver).execute(songs.toArray(new Song[songs.size()]));
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
