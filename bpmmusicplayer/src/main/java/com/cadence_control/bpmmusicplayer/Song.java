package com.cadence_control.bpmmusicplayer;

/**
 * Created by brad on 10/2/14.
 */
public class Song {
    private int trackNum;
    private int BPM;
    private String artist;
    private String album;
    private String title;
    private String songUri;

    public Song(int trackNum, String artist, String album, String title, String songUri) {
        //TODO: Add BPM detection

        this.trackNum = trackNum;
        this.artist = artist;
        this.album = album;
        this.title = title;
        this.songUri = songUri;
    }

    //TODO: Remove this
    public Song(String title) {
        this.title = title;
    }

    public Song(String title, String songUri) {
        this.title = title;
        this.songUri = songUri;
    }

    public int getTrackNum() {
        return trackNum;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public String getSongUri() {
        return songUri;
    }
}
