package com.cadence_control.bpmmusicplayer;

import java.util.ArrayList;

/**
 * Created by brad on 12/4/14.
 */
public class CustomPlaylist {
    private ArrayList<Song> PlaylistSongs;
    private String PlaylistTitle;
    private boolean IsCurated;
    private boolean IsModified = false;

    public void removeSong(int id) {

    }

    public void addSong(Song song) {
        PlaylistSongs.add(song);
        IsModified = true;
    }

    public void savePlaylist(String title) {
        this.PlaylistTitle = title;
        //Figure out how to serialize this later
    }

    public ArrayList<Song> getPlaylistSongs() {
        return PlaylistSongs;
    }

    public String getPlaylistTitle() {
        return PlaylistTitle;
    }
}
