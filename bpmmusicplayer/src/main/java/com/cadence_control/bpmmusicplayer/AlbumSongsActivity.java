package com.cadence_control.bpmmusicplayer;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import com.cadence_control.bpmmusicplayer.cursors.SongsFetcher;

/**
 * Displays all songs of a specific album in a basic ListView.
 *
 * @author Brad Whitfield
 */
public class AlbumSongsActivity extends ListActivity {
    public static final String SELECTED_ALBUM_ID = "selected_album_id";
    public static final String SELECTED_ALBUM = "selected_album";
    private String album;
    private long albumID;

    /**
     * An implementation of Android's default onCreate for list fragments. Using the album ID and name passed
     * in the bundle, the view will get all songs on an album and display them in a list view, and it will set
     * the actionbar heading to the album name.
     *
     * @param savedInstanceState    Default parameter for Android's onCreate() method.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        albumID = intent.getLongExtra(SELECTED_ALBUM_ID, 0);
        album = intent.getStringExtra(SELECTED_ALBUM);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setTitle(album);
        }

        setListAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,
                SongsFetcher.getAllSongsOnAlbum(this.getContentResolver(), albumID)));

        super.onCreate(savedInstanceState);
    }
}
